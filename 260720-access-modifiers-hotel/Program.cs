﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Clients;
using Hotel;

namespace _260720_access_modifiers_hotel
{
    class Program
    {
        static void Main(string[] args)
        {
            Guest aGuest = new Guest(1, "papa");
            Guest bGuest = new Guest(2, "BABA");
            Guest cGuest = new Guest(3, "C");
            Guest dGuest = new Guest(4, "CA");
            Guest eGuest = new Guest(5, "CAS");
            Guest fGuest = new Guest(6, "CASA");


            Console.WriteLine("-------------First--------------");
            Room aRoom = Reception.Checkin(aGuest);
            CwRoom(aRoom);

            Room bRoom = Reception.Checkin(bGuest);
            CwRoom(bRoom);

            Room cRoom = Reception.Checkin(cGuest);
            CwRoom(cRoom);

            Room dRoom = Reception.Checkin(dGuest);
            CwRoom(dRoom);

            Room eRoom = Reception.Checkin(eGuest);
            CwRoom(eRoom);

            Room fRoom = Reception.Checkin(fGuest);
            CwRoom(fRoom);

            Console.WriteLine("-------------First End----------\n");

            Reception.PrintAllRooms();

            Console.WriteLine("-------------CheckOut-----------");
            Console.WriteLine("room before checkout ");
            CwRoom(cRoom);

            Console.WriteLine("room after checkout ");
            Reception.Checkout(cRoom);
            CwRoom(cRoom);


            Reception.PrintAllRooms();

            Console.WriteLine("-------------CheckOut End-------\n");

            
            Console.WriteLine("-------------Sec Start----------");
            fRoom = Reception.Checkin(fGuest);
            CwRoom(fRoom);
            Reception.PrintAllRooms();
            Console.WriteLine("-------------Sec End------------\n");


        }

        static void CwRoom(Room room)
        {
            if (room != null)
            {
                Console.WriteLine(room.ToString());
            }
            else
            {
                Console.WriteLine("Room is null - no instance");
            }
        }
        
    }
}
