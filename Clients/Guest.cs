﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clients
{
    public class Guest
    {
        private int _id;
        private string _name;

        public Guest(int id , string name)
        {
            SetID(id);
            SetName(name);
        }

        private void SetID(int id)
        {
            _id = id; 
        }
        public int GetId()
        {
            return _id ;
        }
        private void SetName(string name)
        {
            _name = name;
        }
        public string GetName()
        {
            return _name ;
        }
    }
}
