﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clients;
namespace Hotel
{
    //internal ~
    //protected #
    //private -
    //public + 

    public abstract class Room
    {
        //Also because of question 3 that asks to enter a price for a room in a constructor
        //- it is not necessary because the price is uniform according to the question
        public Room()
        {
            
        }
        public Room(int roomNumber)
        {
            SetRoomNumber(roomNumber);
        }
        public Room(int roomNumber , int price)
        {
            SetRoomNumber(roomNumber);
            SetPrice(price);
        }

        protected int _roomNumber;
        protected float _price;
        protected Guest _guest;
        protected bool _isClean;
        protected string _features;

        internal void SetGuset(Guest guest)
        {
            _guest = guest;
        }

        protected virtual void SetPrice()
        {
            _price = CalcRoomPrice(50, 25);
        }

        protected int CalcRoomPrice(int basePrice, int extraPriceInTheSummerSeason)
        {
            int price = basePrice;
            switch (DateTime.Now.Month)
            {
                case 7:
                case 8:
                    price += extraPriceInTheSummerSeason;
                    break;
            }

            return price;
        }

        //Also because of question 3 that asks to enter a price for a room in a constructor
        //- it is not necessary because the price is uniform according to the question
        protected virtual void SetPrice(int price)
        {
            _price = price;
        }
        protected abstract void SetFeatures();

        public void SetRoomNumber(int roomNumber)
        {
             _roomNumber = roomNumber ;
        }

        // add to complte the ex
        internal void SetIsCleanRoom(bool clean)
        {
            _isClean = clean;
        }
        public int GetRoomNumber()
        {
            return _roomNumber;
        }

        public float GetPrice()
        {
            return _price;
        }

        public Guest GetGuest()
        {

            return _guest;
        }

        public bool IsClean()
        {
            return _isClean;
        }

        public string GetFeatures()
        {
            return _features;
        }

        public override string ToString()
        {
            string guest = "";
            if (GetGuest() != null)
            {
                guest = $"Guest: {GetGuest().GetName()} ID: {GetGuest().GetId()}";
            }
            else
            {
                guest = $"Guest: No Guest ID: No ID";
            }
            return $"{base.ToString()} Room Number: {GetRoomNumber()}, price: {GetPrice()} , Room is Clean: {IsClean()} features: {GetFeatures()} {guest}";
        }
    }
}
