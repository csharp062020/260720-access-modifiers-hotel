﻿namespace Hotel
{
    class RoomForTwo : Room
    {
        public RoomForTwo()
        {
            SetPrice();
            SetFeatures();
        }
        protected override void SetFeatures()
        {
            _features = "TV, Minibar";
        }
    }
}