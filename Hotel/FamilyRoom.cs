﻿namespace Hotel
{
    class FamilyRoom : Room
    {
        public FamilyRoom()
        {
            SetPrice();
            SetFeatures();
        }
        protected override void SetPrice()
        {
            _price = CalcRoomPrice(100, 25);
        }

        protected override void SetFeatures()
        {
            _features = "TV, Minibar,Big Room";
        }
    }
}