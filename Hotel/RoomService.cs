﻿using System;

namespace Hotel
{
    internal class RoomService
    {
        internal void CleanRoom(RoomForTwo room)
        {
            room.SetIsCleanRoom(true);
            Console.WriteLine($"clean the: {room.GetFeatures()}");
        }

        internal void CleanRoom(FamilyRoom room)
        {
            room.SetIsCleanRoom(true);
            Console.WriteLine($"clean the: {room.GetFeatures()}");
        }

        internal void CleanRoom(Suite room)
        {
            room.SetIsCleanRoom(true);
            Console.WriteLine($"clean the: {room.GetFeatures()}");
        }

    }
}