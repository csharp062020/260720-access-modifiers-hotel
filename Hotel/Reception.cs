﻿using System;
using Clients;

namespace Hotel
{
   public static class Reception
    {
        private const int _numberOfRooms = 5;
        // not ask for
        private static int _numberOfTwo = 2;
        private static int _numberOfSuit = 1;
        private static int _numberOfFamily = 2;
        //----
        private static int _numberOfGuests = 0;
        private static Room[] _rooms= new Room[_numberOfRooms];
        private static RoomService roomService= new RoomService();

        static Reception()
        {
            SetRooms(ref _rooms);
        }

        private static void SetRooms(ref Room[] rooms)
        {
            for (int i = 0; i < rooms.Length; i++)
            {
                if (rooms[i] == null)
                {
                    if (_numberOfTwo != 0)
                    {
                        rooms[i] = new RoomForTwo();
                        _numberOfTwo--;
                    }
                    else if (_numberOfFamily != 0)
                    {
                        rooms[i] = new FamilyRoom();
                        _numberOfFamily--;
                    }
                    else if (_numberOfSuit != 0)
                    {
                        rooms[i] = new Suite();
                        _numberOfSuit--;
                    }

                    rooms[i].SetRoomNumber(i + 1);
                }
            }
        }
        public static Room Checkin(Guest guest)
        {
            for (int i = 0; i < _rooms.Length ; i++)
            {
                if (IsRoomFree(_rooms[i]))
                {
                    _rooms[i].SetGuset(guest);
                    _numberOfGuests++;
                    //not ask for this 
                    _rooms[i].SetIsCleanRoom(false);
                    
                    return _rooms[i];
                }
            }

            Console.WriteLine("No Empy Rooms , the Hotel is Full");
            return null;
        }
        
        public static void Checkout(Room room)
        {
            SendRoomService(room);
            room.SetGuset(null);
            _numberOfGuests--;
        }

        public static bool IsRoomFree(Room room)
        {
            return room.GetGuest() == null;
        }


        private static void SendRoomService(Room room)
        {
            switch (room)
            {
                case FamilyRoom familyRoom:
                    roomService.CleanRoom(familyRoom);
                    break;
                case RoomForTwo roomForTwo:
                    roomService.CleanRoom(roomForTwo);
                    break;
                case Suite suite:
                    roomService.CleanRoom(suite);
                    break;
            }
        }
        public static void PrintAllRooms()
        {
            Console.WriteLine("\n--------Print All Rooms---------\n");

            foreach (Room room in _rooms)
            {
                Console.WriteLine(room.ToString());
            }
            Console.WriteLine("--------Print All Rooms End-----\n");

        }
    }
}