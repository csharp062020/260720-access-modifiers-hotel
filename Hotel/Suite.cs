﻿namespace Hotel
{
    class Suite : Room
    {
        public Suite()
        {
            SetPrice();
            SetFeatures();
        }
        protected override void SetPrice()
        {
            _price = CalcRoomPrice(150, 25);

        }
        protected override void SetFeatures()
        {
            _features = "TV, Minibar, Big Room , Jaquzi , Spa";
        }
    }
}